<footer class="app-footer">
        <div class="d-block w-100 text-center"><strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} </strong> @lang('strings.backend.general.all_rights_reserved') <p class="float-right mb-0">Powered By <a class="mr-4">CAMP 404</a></p></div>

</footer>
